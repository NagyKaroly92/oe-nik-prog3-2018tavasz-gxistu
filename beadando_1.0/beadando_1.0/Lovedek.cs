﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace beadando_1._0
{
    class Lovedek
    {
        Point center;
        
        

        public Point Center { get => center; set => center = value; }

        public Lovedek(Point center, Size jatekter, ref List<Enemy> enemies)
        {
            this.center = center;
            Mozgas(jatekter, ref enemies);
        }

        public void Mozgas(Size jatekter, ref List<Enemy> enemies)
        {
            
            while (center.Y > 0)
            {
                Thread.Sleep(10);
                center.Y -= 10;
                if (Talalat(enemies, this) != null)
                {
                    enemies.Remove(Talalat(enemies, this));
                    center.Y = 0;
                    
                }

            }
                
        }

        private Enemy Talalat( List<Enemy> enemies, Lovedek lovedek)
        {
            
            foreach (Enemy item in enemies)
            {
                if (lovedek.center.X > item.Center.X && lovedek.Center.X < item.Center.X+item.Oldalhossz && lovedek.Center.Y == item.Center.Y)
                {
                    return item;
                }
                
            }
            return null;

            
        }

    }
}
