﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace beadando_1._0
{
    class Enemy
    {

        Point center;
        int oldalhossz;
        Size jatektermeret;
        int irany = 1;
        int szamlalo = 0;
        //Vector sebesseg;

        public Enemy(Point kezdo, int oldalhossz, Size jatektermeret )
        {
            this.Oldalhossz = oldalhossz;
            Center = new Point(kezdo.X / 2, kezdo.Y / 2);
            this.jatektermeret = jatektermeret;
        }

        public Point Center { get => center; set => center = value; }
        //public Point Center1 { get => center; set => center = value; }
        public int Oldalhossz { get => oldalhossz; set => oldalhossz = value; }
        public int Irany { get => irany; set => irany = value; }

        

        public void Mozog()
        {
            center.X += irany * 10;
            szamlalo++;
            if (szamlalo == 6)
            {
                irany = irany*-1;
                if (irany == 1)
                {
                    center.Y += 10;
                }
                szamlalo = 0;
            }

        }

    }
}
