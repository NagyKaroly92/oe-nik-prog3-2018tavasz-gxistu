﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando_1._0
{
    class Jatekter : FrameworkElement
    {
        ImageBrush enemybrush;
        ImageBrush youarebrush;
        ImageBrush lovedekbrush;
        ImageBrush hatterbrush;

        

        ViewModel VM;
        
        public void VMBeallit(ViewModel VM)
        {
            this.VM = VM;
            enemybrush = new ImageBrush(new BitmapImage(new Uri("fighter12.png", UriKind.Relative)));
            youarebrush = new ImageBrush(new BitmapImage(new Uri("goodfighter.png", UriKind.Relative)));
            lovedekbrush = new ImageBrush(new BitmapImage(new Uri("lovedek.png", UriKind.Relative)));
            hatterbrush = new ImageBrush(new BitmapImage(new Uri("hatter1.jpg", UriKind.Relative)));
        }

        protected override void OnRender(DrawingContext drawingContext)
        {

            drawingContext.DrawRectangle(hatterbrush, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));

            if (VM != null)
            {
                foreach (Enemy item in VM.Enemies)
                {
                    drawingContext.DrawRectangle(enemybrush, null, new Rect(new Point(item.Center.X, item.Center.Y), new Point(item.Center.X+20, item.Center.Y+20)));
                }
                drawingContext.DrawRectangle(youarebrush, null, new Rect(VM.Jat.Center, new Size(60, 60)));

                foreach (Lovedek item in VM.Lovedekek)
                {
                    drawingContext.DrawEllipse(lovedekbrush, null, item.Center, 5, 5);
                }
            }
            
        }

    }
}
