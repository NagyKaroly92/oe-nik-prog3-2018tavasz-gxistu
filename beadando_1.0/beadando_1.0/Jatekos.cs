﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace beadando_1._0
{
    class Jatekos
    {

        Point center;
        int oldalhossz = 50;
        int hp;
        Size payameret = new Size(800,450);

        public Jatekos(Point center)
        {
            this.Center = center;
        }

        public Point Center { get => center; set => center = value; }

        /*public void Mozog(int dx, int dy)
{

center.X += dx;
center.Y += dy;

}*/

        public void Mozog(int dx, int dy)
        {
            dx *= 5; dy *= 5;
            if (BentVanE(center.X +dx , oldalhossz + dx))
            {
                center.X += dx;
                center.Y += dy;
            }
        }

        private bool BentVanE(double x, double w)
        {
            return (x >= 0 && x + w < payameret.Width);
        }

    }
}
