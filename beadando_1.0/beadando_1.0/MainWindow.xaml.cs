﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace beadando_1._0
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel VM;
        public MainWindow()
        {
            InitializeComponent();
            VM = new ViewModel();
            jatekter.VMBeallit(VM);
            DispatcherTimer dt = new DispatcherTimer();
            DispatcherTimer dt2 = new DispatcherTimer();
            dt.Interval = TimeSpan.FromMilliseconds(350);
            dt2.Interval = TimeSpan.FromMilliseconds(300);
            dt.Tick += Dt_Tick;
            dt2.Tick += Dt2_Tick;
            dt.Start();
            dt2.Start();
        }

        private void Dt2_Tick(object sender, EventArgs e)
        {
            foreach (Lovedek item in VM.Lovedekek)
            {
                //item.Mozgas(new Size(800, 450), ref VM.enemies);
                jatekter.InvalidateVisual();
            }
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            
                foreach (Enemy item in VM.Enemies)
                {
                    item.Mozog();
                    jatekter.InvalidateVisual();
                }
            
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            int dx = 0, dy = 0;
            switch (e.Key)
            {
                case Key.Left: dx = -1; break;
                case Key.Right: dx = 1; break;
                case Key.Space: Lovedek l = new Lovedek(new Point(VM.Jat.Center.X+10, VM.Jat.Center.Y), new Size(800, 450), ref VM.enemies);
                   // VM.Lovedekek.Add(l);
                    break;

            }
            VM.Jat.Mozog(dx, dy);
            jatekter.InvalidateVisual();
        }
    }
}
